#!/bin/bash

# Script to download a set of URLs that require URS Login
# By Nathan Wharton

# Can't change NETRC unless there is some way to tell wget and curl to use a different one
# NETRC contains usernames and passwords
NETRC=~/.netrc

# COOKIES contains the session cookie for authentication
COOKIES=~/.urs_cookies

# URS_HOST is the host handling the authentication
URS_HOST=urs.earthdata.nasa.gov

# Make sure these files exist
for i in $NETRC $COOKIES ; do
    if [ ! -f $i ] ; then
        touch $i
        # If we create it, make sure it is only read/write by user
        chmod 600 $i
    fi
done

if tty -s ; then
    isatty=yes
fi

if grep -q "^machine $URS_HOST" $NETRC ; then
    if [ "$isatty" ] ; then
        echo Using username and password for $URS_HOST found in $NETRC
    fi
else
    if [ ! "$isatty" ] ; then
        echo Please run at a terminal so you can set up your password
        exit 1
    fi
    echo Enter $URS_HOST username:
    read username
    echo Enter $URS_HOST password:
    read -r -s password
    echo Renter $URS_HOST password:
    read -r -s password_again
    if [ "$password" = "$password_again" ] ; then
        echo "machine $URS_HOST login $username password $password" >> $NETRC
    else
        echo Password Mismatch
        exit 1
    fi
fi

# Either set the URLS by some method that expands them into the block below,
# or pass them in on the command line.
if [ $# = 0 ] ; then
    URLS="
    https://ghrc.nsstc.nasa.gov/pub/data/tmi-op/2006/tmiwop_2006.260_daily.tar.gz
    https://ghrc.nsstc.nasa.gov/pub/data/tmi-op/2006/tmiwop_2006.261_daily.tar.gz
    https://ghrc.nsstc.nasa.gov/pub/data/tmi-op/2006/tmiwop_2006.262_daily.tar.gz
    "
else
    URLS="$@"
fi

if type wget >&/dev/null ; then
    for i in $URLS ; do
        if [ $i != ${i%/} ] ; then
            if [ "$isatty" ] ; then
                echo -n directory $i ...
            fi
            recurOpts="-nd -r -l 1 -R index.html"
        else
            if [ "$isatty" ] ; then
                echo -n $i ...
            fi
            recurOpts=
        fi
        wget_output=`wget $recurOpts --no-check-certificate -c --load-cookies $COOKIES --save-cookies $COOKIES --auth-no-challenge=on --keep-session-cookies $i 2>&1`
        result=$?
        if [ "$isatty" ] ; then
            echo
        fi
        if [ $result = 6 ] ; then
            echo Login at $URS_HOST failed.  Please try again and new password will be requested.
            echo Visit https://$URS_HOST to set up or check your account.
            sed -i "/^machine $URS_HOST/d" $NETRC
            exit 1
        elif [ $result = 8 ] ; then
            if echo $wget_output | grep -q 'ERROR 404' ; then
                echo $i does not exist
            else
                echo Please visit https://$URS_HOST/application_search?query=GHRC and approve the GHRC DAAC Application
                exit 1
            fi
        elif [ $result != 0 ] ; then
            echo Failure detected that this script does not handle:
            echo Exit Code was $result
            echo $wget_output
            exit 1
        fi
    done
elif type curl >&/dev/null ; then
    for i in $URLS ; do
        echo -n $i ...
        curl_output=`curl -k -f -b $COOKIES -c $COOKIES -L -n -o $(basename $i) -C - $i 2>&1`
        result=$?
        echo
        if [ $result = 22 ] ; then
            if echo $curl_output | grep -q '403 Forbidden' ; then
                echo Please visit https://$URS_HOST/application_search?query=GHRC and approve the GHRC DAAC Application
                exit 1
            elif echo $curl_output | grep -q '404 Not Found' ; then
                echo $i does not exist
            else
                echo Login at $URS_HOST failed.  Please try again and new password will be requested.
                echo Visit https://$URS_HOST to set up or check your account.
                sed -i "/^machine $URS_HOST/d" $NETRC
                exit 1
            fi
        elif [ $result = 33 ] ; then
            echo $i previously downloaded
        elif [ $result != 0 ] ; then
            echo Failure detected that this script does not handle:
            echo Exit Code was $result
            echo $curl_output
            exit 1
        fi
    done
else
    echo This script needs wget or curl to be installed
    exit 1
fi
